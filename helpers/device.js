var Device = require('../models/device')

exports.findByDeviceId = (device_id, callback) => Device.findOne({device_id: device_id}, callback)
