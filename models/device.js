var mongoose = require('mongoose')
var Schema = mongoose.Schema

var deviceSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  device_id: {
    type: String,
    required: true,
    unique: true
  },
  device_id_as_int: Number,
  enabled: Boolean,
  balance_last: Number,
  batt_last: Number,
  delta_to_send: Number,
  session_last: Date,
  tilts_last: Number,
  message_last: String,
  name: String
})

module.exports = mongoose.model('device', deviceSchema)
