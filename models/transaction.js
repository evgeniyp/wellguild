var mongoose = require('mongoose')
var Schema = mongoose.Schema

var transactionSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  device_id: {
    type: String,
    required: true
  },
  _device: {
    required: true,
    type: Schema.ObjectId,
    ref: 'device'
  },
  delta: Number
})

module.exports = mongoose.model('balance', transactionSchema)
