var mongoose = require('mongoose')
var Schema = mongoose.Schema

var sessionSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  device_id: {
    type: String,
    required: true
  },
  _device: {
    required: true,
    type: Schema.ObjectId,
    ref: 'device'
  },
  balance_last: Number,
  batt_last: Number,
  delta_sent: Number,
  tilts_last: Number,
  message_last: String
})

module.exports = mongoose.model('session', sessionSchema)
