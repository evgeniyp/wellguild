'use strict'
var async = require('async')
var auth = require('http-auth')
var basic = auth.basic({file: __dirname + '/users.htpasswd'})
var bodyParser = require('body-parser')
var express = require('express')
var favicon = require('serve-favicon')
var logger = require('morgan')
var moment = require('moment')
moment.locale('ru')
var momentEn = require('moment')
momentEn.locale('en')
var helpers = { mmnt: (val) => val ? moment(val).utcOffset(180).format('DD.MM.YYYY HH:mm') : '-' }

var path = require('path')

var config = require('./config')
var connection = require('./connection')

var Device = require('./models/device')
var Transaction = require('./models/transaction')
var Session = require('./models/session')

var deviceHelper = require('./helpers/device')

var app = express()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.set('view engine', 'pug')
app.use(favicon(__dirname + '/public/images/favicon.ico'))
app.use(logger('dev'))
app.use(logger('common', {stream: require('fs').createWriteStream('./access.log', { flags: 'a' })}))
app.use(express.static(path.join(__dirname, 'public')))
app.locals.moment = moment
app.locals.pretty = app.get('env') === 'development'

function getEnTime () {
  var moscow = momentEn().utcOffset(180)

  var d = moscow.format('MMM DD YYYY')
  var t = moscow.format('HH:mm:ss')

  return 'date=' + d + '\ntime=' + t
}

app.get('/', auth.connect(basic), (req, res) => {
  connection.connect(config.mongoUrl)
  var devices = []
  async.waterfall([
    (next) => Device.find().sort({ device_id_as_int: 1 }).exec(next),
    (result, next) => {
      devices = result
      next()
    }],
    err => res.render('index', { devices: devices, err: err, helpers: helpers })
  )
})

app.get('/partial/devices', auth.connect(basic), (req, res) => {
  connection.connect(config.mongoUrl)
  var devices = []
  async.waterfall([
    (next) => Device.find(next),
    (result, next) => {
      devices = result
      next()
    }],
    err => {
      res.render('partials/devices.pug', { devices: devices, err: err, helpers: helpers })
    }
  )
})

app.get('/stamp/:device_id', auth.connect(basic), (req, res) => {
  connection.connect(config.mongoUrl)
  var device_id = req.params.device_id
  var devices = []
  var transactions = []
  var sessions = []
  async.waterfall([
    next => {
      if (device_id === '' || device_id == null) return next('device_id not found')
      Device.findOne({ device_id: device_id }, next)
    },
    (result, next) => {
      if (!result) return next('device not found')
      devices = [result]
      Transaction.find({ _device: devices[0]._id }).sort({ created: -1 }).limit(100).exec(next)
    },
    (result, next) => {
      transactions = result
      Session.find({ _device: devices[0]._id }).sort({ created: -1 }).limit(100).exec(next)
    },
    (result, next) => {
      sessions = result
      next()
    }],
    err => res.render('stamp', { devices: devices, transactions: transactions, sessions: sessions, err: err, helpers: helpers })
  )
})

app.post('/rename/:device_id', auth.connect(basic), urlencodedParser, (req, res) => {
  connection.connect(config.mongoUrl)
  var device_id = req.params.device_id
  var name = req.body.name
  async.waterfall([
    (next) => {
      if (device_id === '' || device_id == null) return next('device_id not found')
      if (!name) return next('name is empty')
      deviceHelper.findByDeviceId(req.params.device_id, next)
    },
    (device, next) => {
      if (!device) return next('device not found')
      device.name = name
      device.save(next)
    }
  ],
    _ => res.redirect(req.headers['referer'])
  )
})

app.post('/delta/:device_id', auth.connect(basic), urlencodedParser, (req, res) => {
  connection.connect(config.mongoUrl)
  var device_id = req.params.device_id
  var delta = parseInt(req.body.delta, 10)
  async.waterfall([
    (next) => {
      if (device_id === '' || device_id == null) return next('device_id not found')
      if (isNaN(delta)) return next('delta not found')
      deviceHelper.findByDeviceId(req.params.device_id, next)
    },
    (device, next) => {
      if (!device) return next('device not found')
      if (isNaN(device.delta_to_send)) device.delta_to_send = 0
      device.delta_to_send += delta
      device.save(next)
    },
    (device, affected, next) => {
      new Transaction({
        device_id: device.device_id,
        _device: device._id,
        delta: delta
      }).save(next)
    }
  ],
    _ => res.redirect(req.headers['referer'])
  )
})

// HTTP GET /sync?device_id=<>&balance=<>&batt=<>[?tilts=<>][?message=<>]
app.get('/sync', (req, res) => {
  connection.connect(config.mongoUrl)
  var device_id = req.query.device_id
  var balance = parseFloat(req.query.balance)
  var batt = parseFloat(req.query.batt)
  var tilts = parseInt(req.query.tilts, 10)
  var message = req.query.message
  var reboot_to_send = 0
  var delta_to_send = 0

  async.waterfall([
    (next) => {
      if (device_id === '' || device_id == null) return next('device_id not found')
      if (isNaN(balance)) return next('balance not found')
      if (isNaN(batt)) return next('batt not found')
      deviceHelper.findByDeviceId(device_id, next)
    },
    (device, next) => {
      if (!device) {
        new Device({ device_id: device_id, enabled: true, device_id_as_int: parseInt(device_id, 10) }).save(next)
      } else {
        next(null, device, 0)
      }
    },
    (device, affected, next) => {
      if (!device.enabled) return next('device disabled')
      if (!isNaN(device.delta_to_send)) delta_to_send = device.delta_to_send

      device.balance_last = balance + delta_to_send
      device.batt_last = batt
      device.delta_to_send = 0
      device.session_last = Date.now()
      device.message_last = message
      if (!isNaN(tilts)) device.tilts_last = tilts
      device.save(next)
    },
    (device, affected, next) =>
      new Session({
        device_id: device_id,
        _device: device._id,
        balance_last: balance,
        batt_last: batt,
        delta_sent: delta_to_send,
        tilts_last: isNaN(tilts) ? null : tilts,
        message_last: message
      }).save(next)
  ],
    (err, session, affected) => {
      if (err) res.status(400).send(err)
      else {
        res.set('Content-Type', 'text/plain')
        res.status(200).send('delta=' + delta_to_send + '\nreboot=' + reboot_to_send + '\n' + getEnTime() + '\n')
      }
    })
})

app.get('/time', (req, res) => {
  res.set('Content-Type', 'text/plain')
  res.status(200).send(getEnTime())
})

app.get('/logout', (req, res) => res.status(401).render('logout'))

app.listen(config.httpPort)
console.log('Listening HTTP on port ' + config.httpPort)
