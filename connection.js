'use strict'
var mongoose = require('mongoose')

exports.connect = (url) => {
  if (mongoose.connection.readyState === 0 || mongoose.connection.readyState === 3) mongoose.connect(url)
}

exports.disconnect = () => {
  mongoose.disconnect()
}
